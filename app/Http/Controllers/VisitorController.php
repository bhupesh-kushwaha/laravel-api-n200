<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApiN200\Service;
use App\Contact;
use App\Events\VisitorEvent;
use App\Visitor;
use Illuminate\Http\Response;

class VisitorController extends Controller
{
    protected $service;

     /**
     * Create a new controller instance.
     *
     * @param Service $service
     * @return void
     */
    public function __construct(Service $service){
        $this->service = $service;
    }

    /**
     * Get visitor by event then save in database
     *
     * @param Service $service
     * @return void
     */
    public function getVisitorByEvent()
    {
        event(new VisitorEvent());

        return Response('All visitors saved successfully', 200);
    }

    public function getAllVisitor()
    {
        $visitor = Visitor::all();

        return Response(
            array(
                'data' => $visitor,
                'message' => 'fetch all visitor lists'
            ),
            200 ) ;
    }

    public function getContactFromVisitor($visitor_code)
    {
        $visitor = Visitor::where('visior_code', $visitor_code)->first();

        $contacts = $visitor->contacts;


        if( empty($contacts) ) {
            $contactDetails = $this->service->contactDetails($visitor->contact_link);

            $firstName = isset($contactDetails['first-name']) && !empty($contactDetails['first-name']) ? $contactDetails['first-name'] : "";
            $lastName = isset($contactDetails['last-name']) && !empty($contactDetails['last-name']) ? $contactDetails['last-name'] : "";
            $company = isset($contactDetails['company']) && !empty($contactDetails['company']) ? $contactDetails['company'] : "";
            $jobFunction = isset($contactDetails['job-function']) && !empty($contactDetails['job-function']) ? $contactDetails['job-function'] : "";
            $email = isset($contactDetails['email']) && !empty($contactDetails['email']) ? $contactDetails['email'] : "";
            
            $contacts = Contact::firstOrCreate([
               'visitor_id' => $visitor->id,
               'first_name' => $firstName,
               'last_name' => $lastName,
               'company' => $company,
               'job_function' => $jobFunction,
               'email' => $email
            ]);
        }

        return Response(
            array(
                'data' => $contacts,
                'message' => 'fetch contact details'
            ),
            200 ) ;

    }
}
