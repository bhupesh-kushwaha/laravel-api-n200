<?php

namespace App\Listeners;

use App\Events\VisitorEvent;
use App\Jobs\Visitorjob;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VisitorEventListener
{
    protected $service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VisitorEvent  $event
     * @return void
     */
    public function handle(VisitorEvent $event)
    {
        \Log::info('=== VisitorEventListener  ========');

        dispatch(new Visitorjob());
    }
}
