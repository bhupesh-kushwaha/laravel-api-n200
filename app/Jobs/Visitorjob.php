<?php

namespace App\Jobs;

use App\ApiN200\Service;
use App\Contact;
use App\Visitor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Visitorjob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Illuminate\Support\Facades\Log::info('=== VisitorJob  ========');

        $service = new Service();

        $data = $service->visitorsEventList();

        foreach( $data['visitor'] as $item ){

            $contactLink = $service->visitorDetails($item['@attributes']['link']);

            $visitor = Visitor::firstOrCreate([
               'visior_code' => $item['@attributes']['code'],
               'revision_code' => $item['@attributes']['revision'],
               'visitoe_link' => $item['@attributes']['link'],
               'contact_link' => $contactLink['contact']['@attributes']['link'],
            ]);

            \Illuminate\Support\Facades\Log::info('=== Visitor  ========');
            \Illuminate\Support\Facades\Log::info(serialize($visitor));

            $contactDetails = $service->contactDetails($contactLink['contact']['@attributes']['link']);

            $firstName = isset($contactDetails['first-name']) && !empty($contactDetails['first-name']) ? $contactDetails['first-name'] : "";
            $lastName = isset($contactDetails['last-name']) && !empty($contactDetails['last-name']) ? $contactDetails['last-name'] : "";
            $company = isset($contactDetails['company']) && !empty($contactDetails['company']) ? $contactDetails['company'] : "";
            $jobFunction = isset($contactDetails['job-function']) && !empty($contactDetails['job-function']) ? $contactDetails['job-function'] : "";
            $email = isset($contactDetails['email']) && !empty($contactDetails['email']) ? $contactDetails['email'] : "";


            $contacts = Contact::firstOrCreate([
               'visitor_id' => $visitor->id,
               'first_name' => $firstName,
               'last_name' => $lastName,
               'company' => $company,
               'job_function' => $jobFunction,
               'email' => $email
            ]);

            \Illuminate\Support\Facades\Log::info('=== contacts  ========');
            \Illuminate\Support\Facades\Log::info(serialize($contacts));
        }
    }
}
