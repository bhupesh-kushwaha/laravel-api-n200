<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'visior_code',
       'revision_code',
       'visitoe_link',
       'contact_link'
    ];

    public function contacts()
    {
        return $this->hasOne(Contact::class, 'visitor_id','id');
    }
}
