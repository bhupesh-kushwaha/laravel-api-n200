<?php

namespace App\ApiN200;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use SimpleXMLElement;

class Service {

    const API_N200_URL = 'https://api.n200.com/';
    const API_N200_EVENT_CODE = '3acjh22b0jhfj';
    const API_N200_AUTHORIZATION_USERNAME = '91adefd0-b042-463e-97b9-be2d54459fbf';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->client = new Client();
    }

    /**
     * Convert xml response data to readbabled format
     *
     * @param object $response
     * @return srtring
     */
    protected function parseResponse ($response) {
        return new SimpleXMLElement($response->getBody()->getContents());
    }

    /**
     * Call api and get collection
     *
     * @param striung $uri
     * @return object
     */
    public function requestApi($uri) {
        try {

            $url = $this::API_N200_URL  . $uri;

            $response = $this->client->request( 'GET', $url, [
                    'auth'    => [
                        $this::API_N200_AUTHORIZATION_USERNAME,
                        ''
                    ],
                    'headers' => [
                        'Content-Type' => 'application/xml'
                    ]
                ]
            );

            $xml = $this->parseResponse($response);

            $json = json_encode($xml);

            $array = json_decode($json, true);

            return collect($array);

        } catch (ClientException $e) {
            return $e->getMessage();
        }
    }

     /**
     * get events visitors
     *
     * @param striung $uri
     * @return object
     */
    public function visitorsEventList() {
        $uri =  'visitors?event=' .$this::API_N200_EVENT_CODE . '&limit=50';

        return $this->requestApi($uri);
    }

    /**
     * get Visitor details by event code
     *
     * @param striung $uri
     * @return object
     */
    public function visitorDetails($uri)
    {
        return $this->requestApi($uri);
    }

    /**
     * get contact detail by contct code
     *
     * @param striung $uri
     * @return object
     */
    public function contactDetails($uri)
    {
        return $this->requestApi($uri);
    }
}
