<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('visitor/event', 'VisitorController@getVisitorByEvent')->name('visitor.event');

Route::get('visitor/list', 'VisitorController@getAllVisitor')->name('visitor.list');

Route::get('visitor/contact/{visitor_code}', 'VisitorController@getContactFromVisitor')->name('visitor.contact.details');
